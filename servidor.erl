% http://localhost:8000/api/servidor:clienteJson/3/-5/2/8

% http://localhost:8000/api/servidor:clienteJson/1/-13/-14/8


% http://localhost:8000/api/servidor:clienteJson/2/9/10/8
-module(servidor).
-export([start/0, clienteJson/3]).


start() ->
	inets:start(httpd, [
		{modules, [
			mod_alias,
			mod_auth,
			mod_esi,
			mod_actions,
			mod_cgi,
			mod_dir,
			mod_get,
			mod_head,
			mod_log,
			mod_disk_log
		]},
		{port,8000},
		{server_name,"shell"},
		{server_root, "D://erlangserver"},
        {document_root, "D://erlangserver/htdocs"},
        {erl_script_alias, {"/api", [servidor]}},
        {error_log, "error.log"},
        {security_log, "security.log"},
        {transfer_log, "transfer.log"},
        {mime_types, [
        	{"html", "text/html"},
        	{"css", "text/css"},
        	{"js", "application/x-javascript"},
            {"json", "application/json"}
        ]}
	]).

clienteJson(SessionID, _Env, _Input) -> 

 	%% metodo para obtener el contenido del _Input
	%Lista= re:slipt(_Input, "/"),
	io:format("Data: ~p~n", [_Input]),
	[AA,BB,CC,XX]= re:split(_Input, "/"),
	%%AA, BB, CC, XX son binarios se conprueba con is_binary(BB).
	%%conversion de binarios a entero
	%%binary_to_integer(binaryvalue)

   
	%convertimos a integer

	A = binary_to_integer(AA),
	B = binary_to_integer(BB),
	C = binary_to_integer(CC),
	X = binary_to_integer(XX),
	%%vemos los datos ya convetidos
	io:format("Valor de A : ~p~n", [A]),
    io:format("Valor de B : ~p~n", [B]),
    io:format("Valor de C : ~p~n", [C]),
    io:format("Valor de X : ~p~n", [X]),
	
	%%calculamos la formula general

	Raizanterior=math:pow(B,2)-(4*A*C),
	if
		Raizanterior < 0 ->
			Resulato_string = io_lib:format("<html><body><h1>Formula general y de distacia</h1><h3>Datos ingresados</h3><h4>Datos A = ~p</h4><h4>Datos B = ~p </h4><h4>Datos C = ~p </h4><h4>Datos X = ~p</h4> <h3>Formula General</h3> <h2>La solucion no es real</h2>  </body></html>",[A,B,C,X]);
		true ->
			Raiz= math:sqrt(math:pow(B,2)-(4*A*C)),
			io:format("Raiz : ~p~n", [Raiz]),
			Numerador_positivo=-B + Raiz,
			Numerador_negativo=-B - Raiz,
			Denomindor=2*A,
			ResultadoPosi=Numerador_positivo/Denomindor,
			ResultadoNega=Numerador_negativo/Denomindor,
			io:format("Resulato de la formula general : ~p~n", [ResultadoPosi]),
			io:format("Resulato de la formula general : ~p~n", [ResultadoNega]),
			% calcular la fromula de la distacia 
			%% se toma a X como el valor de tiempo el resultado de la formula general como las velocidad
			Distancia1 = ( X * ResultadoPosi),
			Distancia2 = (X * ResultadoNega ),
			io:format("Resulato de la distancia con X1 : ~p~n", [Distancia1]),
			io:format("Resulato de la distancia con X2 : ~p~n", [Distancia2]),
			
			Resulato_string = io_lib:format("<html><body><h1>Formula general y de distacia</h1><h3>Datos ingresados</h3><h4>Datos A = ~p</h4><h4>Datos B = ~p </h4><h4>Datos C = ~p </h4><h4>Datos X = ~p</h4> <h3>Formula General</h3> <h4>Valor de X1 = ~p</h4> <h4>Valor de X2 = ~p</h4>  <h3>Formula De la Distancia</h3> <h4>Valor de Distancia con X1 = ~p</h4> <h4>Valor de Distancia con X2 = ~p</h4> </body></html>",[A,B,C,X,ResultadoPosi,ResultadoNega,Distancia1,Distancia2])
	end,

	
	
	io:format(Resulato_string),
	%%enviamos los datos a la pagina web


	
    mod_esi:deliver(SessionID,
   ["Content-Type: text/html\r\n\r\n",Resulato_string]
).	



